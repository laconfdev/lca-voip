<?php

namespace xrobau;

class SpamCheck {

	public static $pdo;

	public $delay = 300;  // Once per 5 mins, max.

	public function __construct() {
		if (is_object(self::$pdo)) {
			return;
		}
		$host = \DBHOST;
		$dbname = \DBNAME;
		self::$pdo = new \PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", \DBUSER, \DBPASS, [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ] );
	}

	public function create_tables() {
		$sql = "CREATE TABLE IF NOT EXISTS `spamcheck` ( `hashedemail` char(128) NOT NULL, `lastemail` bigint default 0,
		       PRIMARY KEY (`hashedemail`))";	
		self::$pdo->query($sql);
	}

	public function checkspam($email) {
		$q = self::$pdo->prepare("SELECT `lastemail` FROM `spamcheck` WHERE `hashedemail`=?");
		$q->execute([self::hashemail($email)]);
		$r = $q->fetchAll(\PDO::FETCH_ASSOC);
		if (empty($r[0]) || $r[0]['lastemail'] < time() - $this->delay) {
			$this->updatespam($email);
			return true;
		}
		return false;
	}

	public function updatespam($email) {
		$q = self::$pdo->prepare("INSERT INTO `spamcheck` (`hashedemail`, `lastemail`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `lastemail`=VALUES(`lastemail`)");
		$q->execute([self::hashemail($email), time()]);
	}

	public static function hashemail($email) {
		$h = hash("sha512", "lca2019seed::$email::otherstuff", false);
		if (strlen($h) !== 128) {
			throw new \Exception("Universe error, a sha512 hash is not 128 chars long");
		}
		return $h;
	}

}


