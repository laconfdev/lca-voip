<div class='container'>
  <div class='row'>
    <div class='col s12'>
      <div class='card'>
        <div class='card-content'>
          <span class='card-title'>Pokemon at LCA</span>
          <p>We have set up a half-finished game of 'Pokemon Crystal', and linked it into the LCA Phone system. The character, named <tt>DRPOOPY</tt> has collected 7 of the 8 badges, and has just finished battling the 8th Gym Leader. She was unimpressed with the victory, and has sent <tt>DRPOOPY</tt> to go and beat all the Dragon Pokemon that are in the caves behind the Gym.</p>
	  <p><tt>DRPOOPY</tt>'s rival, <tt>FARTBOX</tt> will be encountered soon, and will challenge <tt>DRPOOPY</tt> to a battle. We need to level up <tt>DRPOOPY</tt>'s pokemon before then, as we are weak against him!</p>
          <p>You can watch this live on <a href='https://twitch.tv/lcaplays'>the LCAPlays Twitch Channel</a>, but be aware that it does have about 10-15 seconds latency. We have a VNC connection directly to the server, for low latency access - please ask about it in IRC.</p>
        </div>
      </div>
    </div>

    <div class='col s12'>
      <div class='card'>
        <div class='card-content'>
          <span class='card-title'>How to join in</span>
	  <p>Dial 99999 from your LCA extension. You will hear a 'beep' when you're connected.</p>
          <p>Push the following buttons to control <tt>DRPOOPY</tt>:
<ul class='browser-default'>
<li>2: Up</li>
<li>8: Down</li>
<li>4: Left</li>
<li>6: Right</li>
<li>7: Start</li>
<li>9: Select</li>
<li>1: A</li>
<li>3: B</li>
</ul></p>
          <p>If you push an incorrect button, you will hear a beep. When you push a valid button, it will be read back to you.</p>

        </div>
      </div>
    </div>
  </div>
</div>


