<div class='row'>
  <div class='col s10 offset-s1'>
    <div class='card'>
      <div class='card-content'>
        <div class='card-title'>Important Warning!</div>
	<p>The LCA2019 VoIP Server <b>is not private</b>. 
All calls are recorded, in case of any nuisance phone calls, and Metadata is retained to comply with Australian Law.  
Audio recordings will be deleted 2 weeks after the conference is finished, but Metadata is legally required to retained for 2 Years.</p>
<br />
<p><b>While SRTP is enabled, it is optional (for compatibility). Please do not share any confidential or private information over this VoIP service!</b></p>
      </div>
      <div class='card-action'>
        <a id="notsecure" href="/">I understand this free VoIP service is not secure</a>
      </div>
    </div>
  </div>
</div>
