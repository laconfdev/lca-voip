<div class='container'>
  <div class='row'>
    <div class='col s12'>
      <div class='card'>
        <div class='card-content'>
          <span class='card-title'>Legal Information (NZ)</span>
	  <p>As this is being provided to an audience primarily in NZ, we are under the legal requirements of <a href='http://www.legislation.govt.nz/act/public/1961/0043/latest/DLM329814.html'>Section 216B of the NZ Crimes act - Prohibition on use of interception devices</a>. This means that we need to notify you that your calls are being recorded, and why.</p>
          <p>As per Part (5), calls are being recorded by <a href='https://twitter.com/xrobau'>Rob Thomas</a> who is the authorized representative of the VoIP provider.</p>
<br/>
	  <p><tt>I, Rob Thomas, assert that call recordings are in compliance with Part (5) of the Crimes Act, 1961, Section 216B. I also attest that all recordings
		 will be deleted two (2) weeks after the end of the conference, unless they are otherwise requested to be preserved by a valid legal request.</tt></p>

<br/>
	  <p>Whilst it is not expected to have anyone listen to the call recordings, it is important that you are aware that <b>no expectation of privacy</b> is to be
             entertained by the end user.</p>
        </div>
      </div>
    </div>
  </div>
</div>


