<!-- Begin Scripts -->
<script type="text/javascript" src="/js/materialize.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- End Scripts -->

<script>
$("#notsecure").on("click", function(e) {
	e.preventDefault();
	console.log(e);
	Cookies.set("isinsecure", "understood");
	window.location.href = window.location.href;
});

function onAC(sel) {
	console.log(sel);
	$.ajax({
		url: "/ajax.php",
		data: { exten: sel },
	}).done(function(d) {
		$(".isdir").removeClass("hide");
		$("#direxten").text(d.exten);
		$("#displayname").text(d.displayname);
		$("#markdown").html(d.markdown);
	});
}

$(document).ready(function(){
  if (typeof window.directory != "undefined") {
    M.Autocomplete.init(document.querySelectorAll('.autocomplete'), { data: window.directory, minLength: 3, limit: 5, onAutocomplete: onAC });
  }
});
</script>


