<?php

namespace xrobau;

use Ramsey\Uuid\Uuid;

class User {

	private $pdo;

	private $tables = [ 
		"users" => [ 
			[ 'name' => 'id', 'type' => 'int', 'length' => '11', 'options' => 'unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY' ],
			[ 'name' => 'username', 'type' => 'varchar', 'length' => '100', 'options' => 'NOT NULL UNIQUE' ],
			[ 'name' => 'name', 'type' => 'varchar', 'length' => '100', 'options' => 'NOT NULL' ],
			[ 'name' => 'email', 'type' => 'varchar', 'length' => '100', 'options' => 'NOT NULL UNIQUE' ],
			[ 'name' => 'password', 'type' => 'varchar', 'length' => '128', 'options' => 'NOT NULL DEFAULT "x"' ],
			[ 'name' => 'reset_token', 'type' => 'varchar', 'length' => '64', 'index' => true ],
			[ 'name' => 'reset_token_expiry', 'type' => 'bigint', 'length' => '16' ],
		],
	];

	public function __construct() {
		$host = \DBHOST;
		$dbname = \DBNAME;
		$this->pdo = new \PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", \DBUSER, \DBPASS, [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ] );
	}

	public function create_tables() {
		foreach ($this->tables as $tablename => $table) {
			$sql = "CREATE TABLE IF NOT EXISTS `$tablename` (";
			foreach($table as $col) {
				$length = ' ('.$col['length'].') ';
				$sql .= '`'.$col['name'].'` '.$col['type'].$length;
				if (isset($col['options'])) {
					$sql .= $col['options'];
				}
				$sql .= ",";
				if (isset($col['index'])) {
					$sql .= "INDEX(".$col['name']."),";
				}
			}
			$sql = substr($sql, 0, -1) .') ENGINE=InnoDB;';
			$this->pdo->query($sql);
		}
	}

	public function create($user) {
		// We shouldn't be given a password. If we weren't,
		// create a token for the user to respond to.
		if (empty($user['password'])) {
			$user['reset_token'] = Uuid::uuid4()->toString();
			// Leave it valid for 24 hours
			$user['reset_token_expiry'] = time() + 86400;
		}

		$sql = 'INSERT INTO `users` (';
		$bindValues = "";
		foreach($user as $col => $value) {
			if ($col !== "id") {
				$sql .= "`$col`,";
				$bindValues = "$bindValues:$col,";
			}
		}
		$bindValues = substr($bindValues, 0, -1);
		$sql = substr($sql, 0, -1) .") VALUES ($bindValues)";
		$pdostat = $this->pdo->prepare($sql);
		foreach($user as $col => $value) {
			if ($col === "password") {
				$pdostat->bindValue(":$col", $this->encrypt($value));
			} else if ($col !== "id") {
				$pdostat->bindValue(":$col", $value);
			}
		}
		$pdores = $pdostat->execute();
		return $user['reset_token'];
	}

	public function encrypt($password) {
		return password_hash($password, \PASSWORD_DEFAULT);
	}

	public function get($login) {
		$sql = 'SELECT * FROM `users` WHERE `username`=?';
		$pdostat = $this->pdo->prepare($sql);
		$pdores = $pdostat->execute([$login]);
		$result = $pdostat->fetchAll();
		return isset($result[0])?$result[0]:false;
	}

	public function getByEmail($email) {
		$sql = 'SELECT * FROM `users` WHERE `email`=?';
		$pdostat = $this->pdo->prepare($sql);
		$pdores = $pdostat->execute([$email]);
		$result = $pdostat->fetchAll();
		return isset($result[0])?$result[0]:false;
	}

	public function update($update,$where) {
		$sql = 'UPDATE `users` SET ';
		foreach($update as $col => $value) {
			$sql .= $col .' = :'.$col.',';
		}
		$sql = substr($sql,0,-1) .' WHERE ';
		foreach($where as $k => $v) {
			$sql .= " `$k` = '$v' AND" ;
		}
		$pdostat = $this->pdo->prepare(substr($sql, 0, -4));
		foreach($update as $col => $value) {
			if ($col === "password") {
				$pdostat->bindValue(":$col", $this->encrypt($value));
			} else {
				$pdostat->bindValue(":$col", $value);
			}
		}
		$pdores = $pdostat->execute();
		return $pdores;
	}

	public function get_reset_token($email) {
		$sql = 'SELECT * FROM `users` WHERE `email`=?';
		$pdostat = $this->pdo->prepare($sql);
		$pdostat->execute([$email]);
		$result = $pdostat->fetchAll();
		if (!isset($result[0])) {
			return false;
		}
		$id = $result[0]['id'];
		$sql = 'UPDATE `users` SET `reset_token`=:reset_token, `reset_token_expiry`=:reset_token_expiry WHERE `id`=:id';
		$pdostat = $this->pdo->prepare($sql);
		$reset_token = Uuid::uuid4()->toString();
		$pdostat->execute(["id" => $id, "reset_token" => $reset_token, "reset_token_expiry" => time() + 86400]);
		return $reset_token;
	}

	public function reset_password($token, $newpassword) {
		if (!$token || strlen($token) < 16) {
			throw new \Exception("Invalid reset token");
		}
		if (strlen($newpassword) < 6) {
			throw new \Exception("Password too short");
		}
		$sql = 'SELECT * FROM `users` WHERE `reset_token`=?';
		$pdostat = $this->pdo->prepare($sql);
		$pdostat->execute([$token]);
		$result = $pdostat->fetchAll();
		if (!isset($result[0])) {
			throw new \Exception("Invalid reset token");
		}

		// Check expiry
		if (time() > $result[0]['reset_token_expiry']) {
			$this->update(["reset_token" => null, "reset_token_expiry" => null], ["id" => $result[0]['id']]);
			throw new \Exception("Expired reset token");
		}

		// If we made it here, we're good to update it.
		$this->update(["password" => $newpassword, "reset_token" => null, "reset_token_expiry" => null], ["id" => $result[0]['id']]);
		return true;
	}

	public function loginViaEmail($email, $password, $sleep = true) {
		$sql = 'SELECT * FROM `users` WHERE `email`=?';
		$pdostat = $this->pdo->prepare($sql);

		$valid = false;

		$pdores = $pdostat->execute([$email]);
		$result = $pdostat->fetchAll();
		if (isset($result[0]) && password_verify($password, $result[0]['password'])) {
			$valid = true;
		}

		if ($valid) {
			$_SESSION['userid'] = $result[0]['id'];
			$_SESSION['username'] = $result[0]['username'];
			$_SESSION['lastaccess'] = time();
			$_SESSION['login'] = TRUE;
		} else {
			$this->logout();
		}

		if ($sleep) {
			self::secretSleep();
		}

		if ($valid) {
			return $result[0]['id'];
		} else {
			return false;
		}
	}

	public function loginViaUsername($username, $password, $sleep = true) {
		$sql = 'SELECT * FROM `users` WHERE `username`=?';
		$pdostat = $this->pdo->prepare($sql);

		$valid = false;

		$pdores = $pdostat->execute([$user]);
		$result = $pdostat->fetchAll();
		if (isset($result[0]) && password_verify($password, $result[0]['password'])) {
			$valid = true;
		}

		if ($valid) {
			$_SESSION['userid'] = $result[0]['id'];
			$_SESSION['username'] = $result[0]['username'];
			$_SESSION['lastaccess'] = time();
			$_SESSION['login'] = TRUE;
		} else {
			$this->logout();
		}

		if ($sleep) {
			self::secretSleep();
		}

		if ($valid) {
			return $result[0]['id'];
		} else {
			return false;
		}
	}

	public static function secretSleep() {
		// Delay until the start of the next second, to avoid any potential timing attacks
		list($msec, $sec) = explode(" ", microtime());
		// Strip 0. off the front of msec, and only return microsecs
		$msec = substr($msec, 2, 6);
		// Now simply subtract it from 1 million to find out how
		// many usecs we need to sleep
		$usl = 1000000 - $msec;
		// And sleep for that long
		usleep($usl);
	}

	public function logout() {
		if (!empty($_SESSION)) {
			unset($_SESSION['userid']);
			unset($_SESSION['username']);
			unset($_SESSION['login']);
			unset($_SESISON['lastaccess']);
		}
	}

	public function isLoggedIn() {
		if (!isset($_SESSION['lastaccess'])) {
			return false;
		}

		// Log out sessions that haven't been used in 24 hours. 86400 seconds.
		$logoutbefore = time() - 86400;

		if ($_SESSION['lastaccess'] < $logoutbefore) {
			$this->logout();
			return false;
		} else {
			$_SESSION['lastaccess'] = time();
		}
		return $_SESSION['userid'];
	}
}
