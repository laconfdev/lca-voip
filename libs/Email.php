<?php

namespace xrobau;

class Mail {

	private $template;
	public $mailer;
	public $message;
	public $forcebody = false;
	public $from = [ "voip@aussievoip.com.au" => "LCA2019 VoIP Server" ];
	private $vars = [];


	public function __construct($token) {
		$filename = __DIR__."/../views/email.php";

		if (!file_exists($filename)) {
			throw new \Exception("Can't load template");
		}

		$this->template = $filename;
		$t = new \Swift_SmtpTransport('mailserver.9r.com.au', 25);
		$this->mailer = new \Swift_Mailer($t);
		$this->message = new \Swift_Message();
		$this->message->setFrom($this->from);
		$this->message->setReturnPath("discard@aussievoip.com.au");
		$this->token = $token;
	}

	public function setVar($k, $v) {
		$this->vars[$k] = $v;
	}

	public function setSubject($sub) {
		$this->message->setSubject($sub);
	}

	public function setTo($to) {
		$this->message->setTo($to);
	}

	public function setBody($body) {
		$this->forcebody = $body;
	}

	public function setFrom($from) {
		$this->message->setFrom($from);
	}

	public function send() {
		$invitetoken = $this->token;
		if ($this->forcebody) {
			$text = $this->forcebody;
		} else {
			foreach ($this->vars as $k => $v) {
				$$k = $v;
			}
			include $this->template;
		}

		$this->message->setBody($text);
		if (isset($html)) {
			$this->message->addPart($html, 'text/html');
		}
		/* 
		if (is_array($attachments)) {
			foreach ($attachments as $name => $contents) {
				$this->message->attach(\Swift_Attachment::newInstance($contents, $name));
			}
		}
		 */
		$this->mailer->send($this->message);
	}
}


