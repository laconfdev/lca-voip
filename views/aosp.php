
<div class='row'>
  <div class='col s12 m10 offset-m1'>
    <div class='card'>
      <div class='card-content'>
        <p>Note that this is <b>NOT RECOMMENDED</b> unless you are ONLY the Conference Wifi, due to it being unreliable. Most people are using Bria (which is available for both Android and IOS, free for 14 days), which also allows you to enable TLS as a lot of NZ Mobile carriers are blocking SIP traffic over 3G/4G</p>
        <br />
        <p class='center'>You can click or tap on the left or right hand side of the window to scroll between images!</p>
      </div>
    </div>
  </div>
</div>


<div class='row'>
  <div class='col s12'>

<div class="carousel" style='height: 600px'>

  <div class='carousel-item' href="#aosp-1">
    <img src="/img/aosp/1.png">
    <p>Select <b>Settings</b> inside the Dialler</p>
  </div>

  <div class='carousel-item' href="#aosp-2">
    <img src="/img/aosp/2.png">
    <p>Select <b>Calls</b></p>
  </div>

  <div class='carousel-item' href="#aosp-3">
    <img src="/img/aosp/3.png">
    <p>Select <b>Calling accounts</b></p>
  </div>

  <div class='carousel-item' href="#aosp-4">
    <img src="/img/aosp/4.png">
    <p>Select <b>SIP accounts</b></p>
  </div>

  <div class='carousel-item' href="#aosp-5">
    <img src="/img/aosp/5.png">
    <p>Enter the credentials on your card, and click Save. The Server Address is sip.linux.conf.au</p>
  </div>

  <div class='carousel-item' href="#aosp-5a">
    <img src="/img/aosp/5a-tcp.png">
    <p>Optionally, Battery life may be improved if you select TCP, instead of UDP</p>
  </div>

  <div class='carousel-item' href="#aosp-6">
    <img src="/img/aosp/6.png">
    <p>Enable 'Receive incoming calls' if you want to receive calls.</p>
  </div>

  <div class='carousel-item' href="#aosp-7">
    <img src="/img/aosp/7.png">
    <p>Enable SIP Calling <b>For all calls</b></p>
  </div>

  <div class='carousel-item' href="#aosp-8">
    <img src="/img/aosp/8.png">
    <p>You can now make a test call. You will be prompted to select SIP, or your SIM.</p>
  </div>

  <div class='carousel-item' href="#aosp-9">
    <img src="/img/aosp/9.png">
    <p>To validate connectivity, make a test call to <b><tt>*43</tt></b></p>
  </div>

</div>


  </div>
</div>

<script>
 document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, { numVisible: 5 });
  });
</script>
