<?php

namespace xrobau;

use PAMI\Message\Action\DBGetAction;
use PAMI\Message\Action\DBPutAction;

class Asterisk {
	public $ami;

	private $isopen = false;

	public function __construct() {

		$options = [
			'host' => \ASTERISKIP,
			'scheme' => 'tcp://',
			'port' => '5038',
			'username' => \AMIUSER,
			'secret' => \AMIPASS,
			'connect_timeout' => 10000,
			'read_timeout' => 10000,
		];

		$this->ami = new \PAMI\Client\Impl\ClientImpl($options);
	}

	public function getCID($exten) {
		if (!$this->isopen) {
			$this->ami->open();
			$this->isopen = true;
		}
		$g = $this->ami->send(new DBGetAction("AMPUSER/$exten", 'cidname'));
		$events = $g->getEvents();
		if (!isset($events[0])) {
			return false;
		}
		return $events[0]->getValue();
	}

	public function setCID($exten, $cidname = "Unconfigured User") {
		if (!$this->isopen) {
			$this->ami->open();
			$this->isopen = true;
		}
		$g = $this->ami->send(new DBPutAction("AMPUSER/$exten", 'cidname', $cidname));
		return $cidname;
	}

	public function getFpbxDB() {
		static $pdo;
		if (!is_object($pdo)) {
			$host = \ASTERISKIP;
			$dbname = "asterisk";
			$pdo = new \PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", \FPBXDBUSER, \FPBXDBPASS, [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ] );
		}
		return $pdo;
	}

	public function enableVM($exten, $cidname, $email = false, $vmpin = "8675309") {
		$post = [ 'apikey' => \FPBXAPIKEY, 'action' => 'enable', 'exten' => $exten, "displayname" => $cidname,
			"email" => $email, "vmpin" => $vmpin ];
		\Requests::post("http://".\ASTERISKIP."/voicemailapi.php", [], $post);
	}

	public function disableVM($exten) {
		$post = [ 'apikey' => \FPBXAPIKEY, 'action' => 'disable', 'exten' => $exten ];
		\Requests::post("http://".\ASTERISKIP."/voicemailapi.php", [], $post);
	}

}


