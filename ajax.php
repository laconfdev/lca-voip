<?php
require 'vendor/autoload.php';
require 'config/config.php';
error_reporting(-1);

header("Content-Type: application/json;charset=utf-8");

if (empty($_REQUEST['exten'])) {
	print "[]";
	exit;
}

if (!preg_match("/(.+) \((\d{5})\)/", $_REQUEST['exten'], $out)) {
	print json_encode($_REQUEST);
	exit;
}

$lca = new xrobau\LCA;

print json_encode($lca->get_user_dir_entry($out[1], $out[2]));
