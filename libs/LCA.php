<?php

namespace xrobau;

use Ramsey\Uuid\Uuid;

class LCA {
	private static $pdo;
	private static $g;

	public function __construct() {
		if (is_object(self::$pdo)) {
			return;
		}
		$host = \DBHOST;
		$dbname = \DBNAME;
		self::$pdo = new \PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", \DBUSER, \DBPASS, [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ] );
	}

	public function __destruct() {
		// If something's used the $g object, we probably need to commit it.
		if (is_object(self::$g)) {
			self::$g->commit();
		}
	}

	// These are used to init everything, and are called in tools
	public function create_tables() {
		$sql = "CREATE TABLE IF NOT EXISTS `uuidlookup` ( `exten` int(11) unsigned NOT NULL, `row` int(11) unsigned DEFAULT NULL,
			`hashedsecret` char(64), `uuid` char(64) DEFAULT NULL, `displayname` char(64), `vmemail` char(64), `directory` tinyint default '0',
			`vmpin` int(11), `gravatar` char(64) default null,
		       	PRIMARY KEY (`exten`), KEY `uuid_idx` (`uuid`), KEY `dir_idx` (`directory`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ";
		self::$pdo->query($sql);
	}

	public function load_from_gdocs() {
		$p = self::$pdo->prepare("INSERT INTO `uuidlookup` (`exten`, `row`, `hashedsecret`) VALUES (?, ?, ?)");
		$g = $this->getGdocs();
		$service = new \Google_Service_Sheets($g->getClient());
		$response = $service->spreadsheets_values->get($g->sheetid, "Public!A2:B1100");
		$v = $response->getValues();
		foreach ($v as $r => $data) {
			$p->execute([ $data[0], $r + 2, password_hash($data[1], \PASSWORD_DEFAULT) ]);
		}
	}

	public function getGdocs() {
		if (!is_object(self::$g)) {
			self::$g = new \LCA\Google();
		}
		return self::$g;
	}

	// When someone posts, we need to generate a UUID and send them an email
	public function send_email($exten, $pass, $email) {
		$p = self::$pdo->prepare("SELECT * FROM `uuidlookup` WHERE `exten`=?");
		$p->execute([$exten]);
		$res = $p->fetchAll(\PDO::FETCH_ASSOC);
		if (empty($res[0])) {
			return;
		}
		if (!password_verify($pass, $res[0]['hashedsecret'])) {
			return;
		}

		// Password matches, so we can do stuff.
		if (empty($res[0]['uuid'])) {
			// Generate a new UUID
			$uuid = Uuid::uuid4()->toString();
			self::$pdo->query("UPDATE `uuidlookup` SET `uuid`='$uuid' WHERE `exten`='$exten'");
			$g = $this->getGdocs();
			$sheet = $g->getSheet("Public");
			$g->setVal($sheet, "E", $res[0]['row'], $uuid);
		} else {
			$uuid = $res[0]['uuid'];
		}
		$e = new \xrobau\Mail($uuid);
		$e->setSubject("LCA2019 VoIP Management");
		$e->setTo($email);
		$e->send();
		return;
	}

	public function get_from_uuid($uuid) {
		static $p;
		static $cache;
		if (!is_object($p)) {
			$p = self::$pdo->prepare("SELECT * FROM `uuidlookup` WHERE `uuid`=?");
		}

		if (!is_array($cache)) {
			$cache = [];
		}

		if (empty($cache[$uuid])) {
			$p->execute([$uuid]);
			$res = $p->fetchAll(\PDO::FETCH_ASSOC);
			$cache[$uuid] = $res[0]??false;
		}
		return $cache[$uuid];
	}

	public function update_displayname($uuid, $rawdisplayname) {
		// Sanitize display name. ASCII, max 32 chars.
		$displayname = substr(preg_replace('/[\x00-\x1F\x22\x26\x27\x3C-\x3E\x7F-\xFF]/', '', $rawdisplayname), 0, 32);
		$user = $this->get_from_uuid($uuid);
		$a = new Asterisk;
		$a->setCid($user['exten'], $displayname);
		$p = self::$pdo->prepare("UPDATE `uuidlookup` SET `displayname`=? WHERE `uuid`=?");
		$p->execute([$displayname, $uuid]);
		$g = $this->getGdocs();
		$sheet = $g->getSheet('Public');
		$g->setVal($sheet, "C", $user['row'], $displayname);
		return $displayname;
	}

	public function update_voicemail($uuid, $rawvmemail, $displayname = "Unconfigured User", $vmpin = null) {
		$vmemail = substr(preg_replace('/[\x00-\x1F\x22\x26\x27\x3C-\x3E\x7F-\xFF]/', '', $rawvmemail), 0, 64);
		$user = $this->get_from_uuid($uuid);
		$a = new Asterisk;

		if (!is_numeric($vmpin) || (int) $vmpin != $vmpin) {
			$vmpin = null;
		}

		if (!$vmpin) {
			$vmemail = '';
			$a->disableVM($user['exten']);
		} else {
			$a->enableVM($user['exten'], $displayname, $vmemail, $vmpin);
		}

		$p = self::$pdo->prepare("UPDATE `uuidlookup` SET `vmemail`=?, `vmpin`=? WHERE `uuid`=?");
		$p->execute([$vmemail, $vmpin, $uuid]);
		if ($user['vmemail'] != $vmemail) {
			$g = $this->getGdocs();
			$sheet = $g->getSheet('Public');
			$g->setVal($sheet, "D", $user['row'], $vmemail);
		}
		return $vmemail;
	}

	public function update_directory($uuid, $directory) {
		$user = $this->get_from_uuid($uuid);
		$p = self::$pdo->prepare("UPDATE `uuidlookup` SET `directory`=? WHERE `uuid`=?");
		if ($directory == "on") {
			$p->execute([1, $uuid]);
			return 1;
		} else {
			$p->execute([0, $uuid]);
			return 0;
		}
	}

	public function update_gravatar($uuid, $newgravatar) {
		$gravatar = substr(preg_replace('/[\x00-\x1F\x22\x26\x27\x3C-\x3E\x7F-\xFF]/', '', $newgravatar), 0, 64);
		$p = self::$pdo->prepare("UPDATE `uuidlookup` SET `gravatar`=? WHERE `uuid`=?");
		$p->execute([trim(strtolower($gravatar)), $uuid]);
		return $gravatar;
	}

	public function update_misctext($uuid, $newmisc) {
		$misc = htmlentities($newmisc, \ENT_QUOTES|\ENT_SUBSTITUTE, "UTF-8", false);
		$p = self::$pdo->prepare("UPDATE `uuidlookup` SET `misctext`=? WHERE `uuid`=?");
		$p->execute([$misc, $uuid]);
		return $misc;
	}

	public function get_all_dir_entries() {
		return self::$pdo->query("SELECT CONCAT(`displayname`, ' (', `exten`, ')'), CONCAT('https://www.gravatar.com/avatar/', MD5(`gravatar`)) FROM `uuidlookup` WHERE `directory`=1")->fetchAll(\PDO::FETCH_KEY_PAIR);
	}

	public function get_user_dir_entry($name, $exten) {
		$p = self::$pdo->prepare("SELECT `displayname`, `exten`, MD5(`gravatar`) as gravatarhash, `misctext` FROM `uuidlookup` WHERE `exten`=? AND `displayname`=? AND `directory`=1");
		$p->execute([$exten, $name]);
		$res = $p->fetchAll(\PDO::FETCH_ASSOC);
		$retarr = [ "displayname" => null, "exten" => null, "gravatar" => null, "markdown" => "" ];

		if (empty($res[0])) {
			return $retarr;
		}
		$retarr["displayname"] = $res[0]['displayname'];
		$retarr["exten"] = $res[0]['exten'];
		$retarr["gravatar"] = $res[0]['gravatarhash'];
		$p = new \Parsedown;
		$p->setSafeMode(true);
		$retarr["markdown"] = str_replace("<ul>", "<ul class='browser-default'>", $p->text($res[0]['misctext']));
		return $retarr;
	}
}

